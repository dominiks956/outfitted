# outfitted-web
Authored by a team of 6:
-Arjan Golabijan
-Dominik Sieradzki
-Fethi Tewelde
-Gabriel Takiye
-Irfaan Opoku
-Yessin el Khaldi

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

